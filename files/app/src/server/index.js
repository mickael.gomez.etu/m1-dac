const express = require('express');

const app = express();

require('express-ws')(app);

app.use(express.json());
app.use(express.urlencoded());

app.use('/admin', require('./admin'));

app.use('/', require('./dashboard'));

app.listen(process.env.PORT || 4242, () => {
  console.log(`Listening on port ${process.env.PORT || 4242}!`);
});
