const router = require('express').Router();
const randomData = require('./modules/randomData');
const getLastCollection = require('./modules/dbManager');
const config = require('./config.json');

router.get('/config', (req, res) => res.send(config));

router.ws('/sensor/:sensorId', (ws, { params }) => {
  const { sensorId } = params;
  let interval = null;

  const stopInterval = () => {
    clearInterval(interval);
    interval = null;
  };

  ws.on('close', () => {
    stopInterval();
  });

  ws.on('message', (req) => {
    const { running, realtime } = JSON.parse(req);
    const { sensors } = config;

    if (!running) {
      stopInterval();
      return;
    }

    if (interval) {
      stopInterval();
      interval = null;
    }

    const { min, max, rate } = sensors.find(sensor => sensor.id === sensorId);

    if (realtime) {
      getLastCollection('thermocouple').then(({ value, time }) => {
        ws.send(JSON.stringify({ mesure: value, dateTime: time }));
      });
      interval = setInterval(() => {
        getLastCollection('thermocouple').then(({ value, time }) => {
          ws.send(JSON.stringify({ mesure: value, dateTime: time }));
        });
      }, rate);
    } else {
      ws.send(JSON.stringify(randomData(min, max)));
      interval = setInterval(() => {
        ws.send(JSON.stringify(randomData(min, max)));
      }, rate);
    }
  });
});

module.exports = router;
