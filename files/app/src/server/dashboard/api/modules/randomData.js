module.exports = (low, high) => ({
  dateTime: new Date().toLocaleTimeString(),
  mesure: (Math.random() * (high - low) + low).toFixed(2)
});
