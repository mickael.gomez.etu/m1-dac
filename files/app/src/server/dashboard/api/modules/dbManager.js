const MongoClient = require('mongodb').MongoClient;

let client;

try {
    MongoClient.connect(`mongodb://root:root@${process.env.MYSQL_HOST_IP}:27017`, (err, cli) => {
        client = cli;
    });
} catch (e) {
    console.log(`MongoClient connect error : ${e}`);
}

const getLastCollection = async (dbName) => {
    const collection = await client.db(dbName).collection('mesures');
    const result = await collection.find().sort({$natural: -1}).limit(1).next();
    return result;
}

module.exports = getLastCollection;
