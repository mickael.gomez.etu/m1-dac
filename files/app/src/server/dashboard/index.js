const express = require('express');

const router = express.Router();

router.use('/api', require('./api'));

router.use('/', express.static('dist'));

module.exports = router;
