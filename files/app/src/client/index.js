import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

fetch('/api/config')
  .then(config => config.json())
  .then(({ sensors }) => {
    ReactDOM.render(<App sensors={sensors} />, document.getElementById('root'));
  });
