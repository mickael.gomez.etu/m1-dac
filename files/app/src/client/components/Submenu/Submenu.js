import React from 'react';
import PropTypes from 'prop-types';
import BoxButton from '../BoxButton/BoxButton';
import './Submenu.css';

const Submenu = ({ active }) => (
  <div className={`sub-menu ${active ? 'active' : ''}`}>
    <div className="button-list">
      <BoxButton
        content="Start"
        color="green"
        anim="up"
        action={() => console.log('...')}
      />
      <BoxButton
        content="Stop"
        color="red"
        anim="up"
        action={() => console.log('...')}
      />
      <BoxButton
        content="Récupérer données"
        color="blue"
        anim="up"
        action={() => console.log('...')}
      />
    </div>
  </div>
);

Submenu.propTypes = {
  active: PropTypes.bool
};

Submenu.defaultProps = {
  active: false
};

export default Submenu;
