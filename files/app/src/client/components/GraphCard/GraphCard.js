import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Chartist from 'chartist';
import InputNumber from '../InputNumber/InputNumber';
import './chartist.min.css';
import './GraphCard.css';

export default class GraphCard extends Component {
  static propTypes = {
    sensor: PropTypes.shape({
      title: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      min: PropTypes.number.isRequired,
      max: PropTypes.number.isRequired,
      rate: PropTypes.number.isRequired,
      scale: PropTypes.number.isRequired,
      unit: PropTypes.string
    }).isRequired,
    toggle: PropTypes.func.isRequired,
    minimized: PropTypes.bool.isRequired,
    running: PropTypes.bool.isRequired,
    realtime: PropTypes.bool.isRequired
  }

  constructor(props) {
    super(props);
    const { sensor } = this.props;
    this.state = {
      maximized: false,
      chart: null,
      data: {
        labels: [...Array(sensor.scale).fill('*')],
        series: [[...Array(sensor.scale).fill(null)]]
      },
      scale: sensor.scale,
      ws: null
    };
  }

  componentDidMount() {
    this.connectSocket();

    // Chart can only be setup after DOM loading
    const { sensor } = this.props;
    const { data } = this.state;
    this.setState({
      chart: new Chartist.Line(`.ct-${sensor.id}`, data, { low: sensor.min, high: sensor.max })
    });
  }

  componentDidUpdate(prevProps) {
    const { minimized, running } = this.props;

    if (prevProps.minimized !== minimized) {
      // Simulate rezize for graph redraw
      window.dispatchEvent(new Event('resize'));
    }

    if (prevProps.running !== running) {
      this.requestData();
    }
  }

  connectSocket = (isReconnect = false) => {
    try {
      const { sensor } = this.props;
      this.setState({
        ws: new WebSocket(`ws://172.28.100.104:80/api/sensor/${sensor.id}`)
      }, () => this.socketEventsManager(isReconnect));
    } catch (e) {
      this.socketReconnection();
    }
  }

  socketEventsManager = (isReconnect = false) => {
    const { ws } = this.state;

    ws.onopen = () => {
      console.log('ws: connected');
      if (isReconnect) { this.requestData(); }
    };

    ws.onclose = () => {
      console.log('ws: disconnected');
      this.socketReconnection();
    };

    ws.onmessage = this.updateChart;
  }

  socketReconnection = () => {
    const { ws } = this.state;
    if (ws) { this.setState({ ws: null }); }
    setTimeout(() => this.connectSocket(true), 5000);
  }

  requestData = () => {
    const { sensor, realtime, running } = this.props;
    const { ws } = this.state;
    ws.send(JSON.stringify({ running, realtime, sensorId: sensor.id }));
  }

  updateChart = ({ data }) => {
    const { dateTime, mesure } = JSON.parse(data);
    const { chart, scale } = this.state;

    // Update labels
    const labels = [...chart.data.labels];
    while (scale <= labels.length) labels.shift();
    labels.push(dateTime);

    // Update series
    const series = [[...chart.data.series[0]]];
    while (scale <= series[0].length) series[0].shift();
    series[0].push(parseFloat(mesure));

    this.setState({ data: { labels, series } }, () => {
      chart.update({ labels, series });
    });
  }

  setScale = (scale) => {
    this.setState({ scale });
  }

  maximize = () => {
    this.setState(state => ({
      maximized: !state.maximized
    }), () => {
      // Simulate rezize for graph redraw
      window.dispatchEvent(new Event('resize'));
    });
  }

  render() {
    const {
      maximized, scale, data, ws
    } = this.state;
    const { sensor, toggle, minimized } = this.props;

    return (
      <div
        className={`
          graph-card
          ${maximized ? 'maximized' : ''}
          ${minimized ? 'minimized' : ''}
          ${ws ? '' : 'disconnected'}
        `}
        id={sensor.id}
      >
        <div className="panel">
          <p className="type">{`Mesure ${sensor.unit || ''}`}</p>
          <p className="title">{sensor.title}</p>
          <button type="button" className="icon-maximize" onClick={this.maximize}>
            {maximized ? '\u2750' : '\u26F6'}
          </button>
          <button type="button" className="icon-minimize" onClick={() => toggle(sensor.id)}>
            &minus;
          </button>
          <p className="value">{data.series[0].slice(-1)[0] || '...'}</p>
          <InputNumber max={50} value={scale} change={this.setScale} />
        </div>
        <div className="data">
          <div className={`ct-${sensor.id}`} />
        </div>
      </div>
    );
  }
}
