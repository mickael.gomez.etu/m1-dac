/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import './Status.css';

export default class Status extends Component {
  // static propTypes = {
  //   content: PropTypes.string.isRequired
  // };

  constructor(props) {
    super(props);
    this.state = {
      connected: true
    };
  }

  toggleActive = () => {
    this.setState(state => ({ active: !state.active }));
  }

  render() {
    const { connected } = this.state;

    return (
      <div className="status">
        <svg className={`fa-database ${connected ? 'connected' : 'disconnected'}`} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="database" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M448 73.143v45.714C448 159.143 347.667 192 224 192S0 159.143 0 118.857V73.143C0 32.857 100.333 0 224 0s224 32.857 224 73.143zM448 176v102.857C448 319.143 347.667 352 224 352S0 319.143 0 278.857V176c48.125 33.143 136.208 48.572 224 48.572S399.874 209.143 448 176zm0 160v102.857C448 479.143 347.667 512 224 512S0 479.143 0 438.857V336c48.125 33.143 136.208 48.572 224 48.572S399.874 369.143 448 336z" /></svg>
      </div>
    );
  }
}
