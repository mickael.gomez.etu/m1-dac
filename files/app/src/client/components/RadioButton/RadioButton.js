/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable object-curly-newline */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import './RadioButton.css';

const RadioButton = ({ content, id, checked, action, disabled }) => (
  <div className={`radio-button ${disabled && !checked ? 'disabled' : ''}`}>
    <input type="radio" id={id} name="selector" defaultChecked={checked} />
    <div className="check" />
    <label htmlFor={id} onClick={e => action({ e, checked })}>
      {content}
    </label>
  </div>
);

RadioButton.propTypes = {
  content: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  action: PropTypes.func.isRequired,
  disabled: PropTypes.bool
};

RadioButton.defaultProps = {
  checked: false,
  disabled: false
};

export default RadioButton;
