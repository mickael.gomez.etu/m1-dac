/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './AdminPanel.css';

export default class AdminPanel extends Component {
  static propTypes = {
    open: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      auth: false
    };
  }

  authenticate = (e) => {
    e.preventDefault();
    const credentials = new FormData(e.currentTarget);

    if (credentials.get('username') !== 'obsrov') {
      this.loginError('Incorrect username');
      return;
    }

    if (credentials.get('password') !== 'vorsbo') {
      this.loginError('Incorrect password');
      return;
    }

    this.setState({ auth: true });
  }

  loginError = (error) => {
    console.log(error);
  }

  render() {
    const { auth } = this.state;
    const { open } = this.props;

    return (
      <div className={`admin-panel ${open ? 'visible' : 'hidden'}`}>
        <div className={`auth ${auth ? 'hidden' : 'visible'}`}>
          <form className="login" onSubmit={this.authenticate}>
            <input type="text" name="username" placeholder="Username" />
            <input type="password" name="password" placeholder="Password" />
            <button type="submit">Login</button>
          </form>
        </div>
        <div className={`panel ${auth ? 'visible' : 'hidden'}`}>
          <p>Coucou</p>
        </div>
      </div>
    );
  }
}
