/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RadioButton from '../RadioButton/RadioButton';
import BoxButton from '../BoxButton/BoxButton';
import './DataLauncher.css';

export default class DataLauncher extends Component {
  static propTypes = {
    toggleRealtime: PropTypes.func.isRequired,
    toggleRunning: PropTypes.func.isRequired,
    running: PropTypes.bool.isRequired,
    realtime: PropTypes.bool.isRequired
  };

  handleButton = ({ active }) => {
    if (!active) {
      const { toggleRunning } = this.props;
      toggleRunning();
      console.log('toggle running');
    }
  };

  handleRadio = ({ e, checked }) => {
    const { running, toggleRealtime } = this.props;
    if (running) {
      e.preventDefault();
      return;
    }
    if (!checked) {
      toggleRealtime();
      console.log('toggle realtime');
    }
  }

  render() {
    const { running, realtime } = this.props;

    return (
      <div className="data-launcher">
        <div className="button-list">
          <RadioButton
            content="Temps réel"
            id="realtime"
            action={this.handleRadio}
            checked={realtime}
            disabled={running}
          />
          <RadioButton
            content="Simulation"
            id="simulation"
            action={this.handleRadio}
            checked={!realtime}
            disabled={running}
          />
        </div>
        <div className="button-list">
          <BoxButton
            content="Start"
            color="green"
            anim="slide"
            data-action="start"
            action={this.handleButton}
            active={running}
          />
          <BoxButton
            content="Stop"
            color="red"
            anim="slide"
            data-action="stop"
            action={this.handleButton}
            active={!running}
          />
        </div>
      </div>
    );
  }
}
