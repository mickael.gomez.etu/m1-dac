/* eslint-disable object-curly-newline */
import React from 'react';
import PropTypes from 'prop-types';
import './BoxButton.css';

const BoxButton = ({ content, color, anim, action, active }) => (
  <button
    type="button"
    className={`box ${active ? 'active' : ''} ${anim}`}
    data-color={color}
    onClick={e => action({ e, active })}
  >
    {content}
  </button>
);

BoxButton.propTypes = {
  content: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  anim: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
  active: PropTypes.bool
};

BoxButton.defaultProps = {
  active: false
};

export default BoxButton;
