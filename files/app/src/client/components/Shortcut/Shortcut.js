/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import './Shortcut.css';

const Shortcut = ({ id, toggle, hidden }) => (
  <div className={`shortcut ${hidden ? 'hidden' : ''}`} id={id} onClick={() => toggle(id)}>
    {id}
  </div>
);

Shortcut.propTypes = {
  id: PropTypes.string.isRequired,
  toggle: PropTypes.func.isRequired,
  hidden: PropTypes.bool.isRequired
};

export default Shortcut;
