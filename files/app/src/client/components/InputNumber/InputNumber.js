import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './InputNumber.css';

export default class InputNumber extends Component {
  static propTypes = {
    value: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    change: PropTypes.func.isRequired
  }

  static defaultProps = {
    value: 0,
    min: 0,
    max: 10
  }

  constructor(props) {
    super(props);
    const { value } = this.props;
    this.state = {
      current: value
    };
  }

  increment = () => {
    const { max, change } = this.props;
    const { current } = this.state;

    if (current >= max) return;

    change(current + 1);
    this.setState({ current: current + 1 });
  }

  decrement = () => {
    const { min, change } = this.props;
    const { current } = this.state;

    if (current <= min) return;

    change(current - 1);
    this.setState({ current: current - 1 });
  }

  render() {
    const { current } = this.state;

    return (
      <div className="input-number">
        <button type="button" onClick={this.decrement}>&minus;</button>
        <span>{current}</span>
        <button type="button" onClick={this.increment}>&#43;</button>
      </div>
    );
  }
}
