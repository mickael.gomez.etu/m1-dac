/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SubMenu from '../Submenu/Submenu';
import './MenuLauncher.css';


export default class MenuLauncher extends Component {
  static propTypes = {
    content: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
  }

  toggleActive = () => {
    this.setState(state => ({ active: !state.active }));
  }

  render() {
    const { content } = this.props;
    const { active } = this.state;

    return (
      <div className={`menu-launcher ${active ? 'active' : ''}`} onClick={this.toggleActive}>
        <div className="playBut">
          <svg x="0px" y="0px" viewBox="0 0 213.7 213.7" enableBackground="new 0 0 213.7 213.7" xmlSpace="preserve">
            <polygon className="triangle" fill="none" strokeWidth="7" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" points="73.5,62.5 148.5,105.8 73.5,149.1 " />
            <circle className="circle" fill="none" strokeWidth="7" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" cx="106.8" cy="106.8" r="103.3" />
          </svg>
        </div>
        <p>{content}</p>
        <SubMenu active={active} />
      </div>
    );
  }
}
