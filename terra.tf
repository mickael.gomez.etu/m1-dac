# Configure the OpenStack Provider
provider "openstack" {}

# Create a web server
resource "openstack_compute_instance_v2" "basic" {
  name            = "terra-instance"
  image_id        = "bf420b24-7df0-485f-ae29-1f778c3d1df4"
  flavor_name     = "moyenne"
  key_pair        = "Gomez keypair"
  security_groups = ["default"]
  network {
    name = "prive"
  }
}